package com.example.android.movieproject.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.android.movieproject.models.Movie;

import java.util.List;

@Dao
public interface MovieDao {

    @Query("SELECT * FROM movie")
    List<Movie> getAll();

    @Query("SELECT * FROM movie WHERE id = :id")
    Movie getMovieById(int id);

    @Insert
    void insertAll(Movie... movies);

    @Query("DELETE FROM movie WHERE id = :id")
    void deleteMovieById(int id);

}
