package com.example.android.movieproject.appDatabase;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.android.movieproject.dao.MovieDao;
import com.example.android.movieproject.models.Movie;

@Database(entities = {Movie.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract MovieDao movieDao();
}
