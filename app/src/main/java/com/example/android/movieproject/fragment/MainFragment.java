package com.example.android.movieproject.fragment;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.SearchManager;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;

import android.content.res.Configuration;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.android.movieproject.BuildConfig;
import com.example.android.movieproject.R;
import com.example.android.movieproject.adapters.MovieListAdapter;
import com.example.android.movieproject.appDatabase.AppDatabase;
import com.example.android.movieproject.application.MoviesApplication;
import com.example.android.movieproject.data.MovieList.OrderedMovies;
import com.example.android.movieproject.data.MovieList.ResultsItem;
import com.example.android.movieproject.data.MovieSearch.MovieSearchResponse;
import com.example.android.movieproject.models.Movie;
import com.example.android.movieproject.service.MovieService;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainFragment extends Fragment implements MovieListAdapter.MovieAdapterOnClickHandler {

    private String MAIN_TAG = "main";
    private String movieQuery;
    private TextView tv_MovieError;
    private ImageView iv_Error;
    private RecyclerView mRecyclerView;
    private MovieListAdapter mMovieListAdapter;
    private FragmentActivity activity;
    private ProgressBar mLoadingIndicator;
    private MovieService service;
    private SwipeRefreshLayout swipeRefreshLayout;
    private AppDatabase db;
    private int rowCount;
    public String chosenOrder = "popular";
    private String STATE_CHOSEN_ORDER = "state_chosen_order";
    private String STATE_LAST_SEARCH = "state_last_search";

    public MainFragment(){
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_main, null);

        if (savedInstanceState != null) {
            // Restore value of members from saved state
            chosenOrder = savedInstanceState.getString(STATE_CHOSEN_ORDER);
            movieQuery = savedInstanceState.getString(STATE_LAST_SEARCH);
        }

        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            rowCount = 4;
        } else {
            rowCount = 2;
        }

        db = ((MoviesApplication)getActivity().getApplication()).getDb();

        activity = getActivity();
        mRecyclerView = (RecyclerView) v.findViewById(R.id.rv_movie);

        //SwipeRefresh
        swipeRefreshLayout = (SwipeRefreshLayout) v.findViewById(R.id.refreshMovieLayout);
        swipeRefreshLayout.setOnRefreshListener(
                () -> {
                    mMovieListAdapter.setMovieData(null);
                    if(chosenOrder.equals("search")){
                        loadMovieSearchData();
                    }
                    if(chosenOrder.equals("local")){
                        loadLocalMovieData();
                    } else {
                        loadMovieData();
                    }
                    swipeRefreshLayout.setRefreshing(false);
                }
        );


        service = ((MoviesApplication)getActivity().getApplication()).getService();

        //RecyclerView
        GridLayoutManager layoutManager
                = new GridLayoutManager(activity,rowCount,GridLayoutManager.VERTICAL,false);
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);
        mMovieListAdapter = new MovieListAdapter(this, this);
        mRecyclerView.setAdapter(mMovieListAdapter);

        tv_MovieError = (TextView) v.findViewById(R.id.tv_movie_error);
        mLoadingIndicator = (ProgressBar) v.findViewById(R.id.pb_loading_indicator);
        showLoading();
        if(chosenOrder.equals("search")){
            loadMovieSearchData();
        }
        else if(chosenOrder.equals("local")){
            loadLocalMovieData();
        } else {
            loadMovieData();
        }
        return v;
    }

    /*@Override
    public void onResume() {
        super.onResume();
        if(chosenOrder.equals("local")){
            loadLocalMovieData();
        }
    }*/


    private void loadMovieData() {

        service.fetchList(chosenOrder, BuildConfig.TMDB_KEY).enqueue(new Callback<OrderedMovies>() {
            @Override
            public void onResponse(Call<OrderedMovies> call, Response<OrderedMovies> response ) {
                List<Movie> movieList = new ArrayList <Movie>();
                OrderedMovies orderedMovies = response.body();
                if(orderedMovies != null) {
                    for (ResultsItem result : orderedMovies.getResults()) {
                        movieList.add(new Movie(result.getId(), result.getBackdropPath()));
                    }
                    mMovieListAdapter.setMovieData(movieList);
                    tv_MovieError.setVisibility(ImageView.INVISIBLE);
                    showMovieDataView();
                } else {
                    tv_MovieError.setText("Something went wrong, please try again or check your connection");
                    tv_MovieError.setVisibility(ImageView.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<OrderedMovies> call, Throwable t ) {
                tv_MovieError.setText("Something went wrong, please try again or check your connection");
                showError();
            }
        });


    }

    public void loadLocalMovieData() {
        tv_MovieError.setVisibility(ImageView.INVISIBLE);
        showLoading();
        Log.w("MainActivity","Calling db now");
        getActivity().runOnUiThread(() -> {
            Executor executor = Executors.newSingleThreadExecutor();
            executor.execute(() -> {
                List<Movie> movieList = db.movieDao().getAll();
                getActivity().runOnUiThread(() -> {
                    if(movieList.size()!=0) {
                        mMovieListAdapter.setMovieData(movieList);
                        showMovieDataView();
                    } else {
                        tv_MovieError.setText("You don't have favorite movies at the moment");
                        showError();
                    }
                });
            });
        });

    }

    private void loadMovieSearchData() {

        service.fetchMovieSearch(BuildConfig.TMDB_KEY,movieQuery).enqueue(new Callback<MovieSearchResponse>() {
            @Override
            public void onResponse(Call<MovieSearchResponse> call, Response<MovieSearchResponse> response ) {
                List<Movie> movieList = new ArrayList <Movie>();
                MovieSearchResponse movieSearch = response.body();
                if(movieSearch != null) {
                    for (com.example.android.movieproject.data.MovieSearch.ResultsItem result : movieSearch.getResults()) {
                        movieList.add(new Movie(result.getId(), result.getBackdropPath()));
                    }
                    mMovieListAdapter.setMovieData(null);
                    mMovieListAdapter.setMovieData(movieList);
                    tv_MovieError.setVisibility(ImageView.INVISIBLE);
                    showMovieDataView();
                } else {
                    tv_MovieError.setText("Something went wrong, please try again or check your connection");
                    tv_MovieError.setVisibility(ImageView.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<MovieSearchResponse> call, Throwable t ) {
                tv_MovieError.setText("Something went wrong, please try again or check your connection");
                showError();
            }
        });


    }

    /**
     * This method is overridden by our MainActivity class in order to handle RecyclerView item
     * clicks.
     *
     * @param movieClicked The movie that was clicked
     */
    @Override
    public void onClick(String movieClicked) {
        Fragment detailFragment = new DetailFragment(movieClicked);

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.mainFragment,detailFragment,"detail");
        transaction.addToBackStack(null);
        transaction.commit();


    }

    /**
     * This method will make the View for the weather data visible and
     * hide the error message.
     * <p>
     * Since it is okay to redundantly set the visibility of a View, we don't
     * need to check whether each view is currently visible or invisible.
     */
    private void showMovieDataView() {
        mLoadingIndicator.setVisibility(View.INVISIBLE);
        tv_MovieError.setVisibility(TextView.INVISIBLE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void showLoading() {
        mRecyclerView.setVisibility(View.INVISIBLE);
        tv_MovieError.setVisibility(TextView.INVISIBLE);
        mLoadingIndicator.setVisibility(View.VISIBLE);
    }

    private void showError() {
        mLoadingIndicator.setVisibility(View.INVISIBLE);
        mRecyclerView.setVisibility(View.INVISIBLE);
        tv_MovieError.setVisibility(TextView.VISIBLE);
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            // Restore value of members from saved state
            chosenOrder = savedInstanceState.getString(STATE_CHOSEN_ORDER);
            movieQuery = savedInstanceState.getString(STATE_LAST_SEARCH);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.movie, menu);

        MenuItem search = menu.findItem(R.id.search_movie);
        SearchView searchView = (SearchView) search.getActionView();
        searchView.setQueryHint("Search movie here!");
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.d("OnQueryTextSubmit",query);
                chosenOrder = "search";
                movieQuery = query;
                if (!"".equals(query)) {
                    searchView.post(() -> searchView.clearFocus());
                }
                loadMovieSearchData();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getOrder();



        switch(id){
            case 2:
                chosenOrder = "popular";
                mMovieListAdapter.setMovieData(null);
                loadMovieData();
                break;
            case 3:
                chosenOrder = "top_rated";
                mMovieListAdapter.setMovieData(null);
                loadMovieData();
                break;
            case 4:
                chosenOrder = "local";
                mMovieListAdapter.setMovieData(null);
                loadLocalMovieData();
                break;
            default:
                Log.w("MainActivity","How did you even do this");
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putString(STATE_CHOSEN_ORDER, chosenOrder);
        savedInstanceState.putString(STATE_LAST_SEARCH,movieQuery);
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            // Restore value of members from saved state
            chosenOrder = savedInstanceState.getString(STATE_CHOSEN_ORDER);
            movieQuery = savedInstanceState.getString(STATE_LAST_SEARCH);
        }

    }

}
