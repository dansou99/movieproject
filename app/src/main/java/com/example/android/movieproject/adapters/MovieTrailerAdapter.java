package com.example.android.movieproject.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.android.movieproject.R;
import com.example.android.movieproject.data.MovieTrailers.MovieTrailerResponse;
import com.example.android.movieproject.data.MovieTrailers.ResultsItem;
import com.example.android.movieproject.fragment.DetailFragment;

public class MovieTrailerAdapter extends RecyclerView.Adapter<MovieTrailerAdapter.MovieTrailerAdapterViewHolder> {
    private MovieTrailerResponse mMovieTrailers;
    private DetailFragment detailActivity;
    private final MovieTrailerAdapterOnClickHandler mClickHandler;

    /**
     * The interface that receives onClick messages.
     */
    public interface MovieTrailerAdapterOnClickHandler {
        void onClickTrailer(String trailerChosen);
    }

    /**
     * Creates a MovieListAdapter.
     *
     * @param clickHandler The on-click handler for this adapter. This single handler is called
     *                     when an item is clicked.
     * @param main
     */
    public MovieTrailerAdapter(MovieTrailerAdapterOnClickHandler clickHandler, DetailFragment main) {
        mClickHandler = clickHandler;
        detailActivity = main;
    }


    public class MovieTrailerAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final ImageView mMovieImageView;
        private final TextView tv_TrailerNum;

        public MovieTrailerAdapterViewHolder(View view) {
            super(view);
            mMovieImageView = (ImageView) view.findViewById(R.id.iv_PlayButton);
            tv_TrailerNum = (TextView) view.findViewById(R.id.tv_trailerNum);
            view.setOnClickListener(this);
        }

        /**
         * This gets called by the child views during a click.
         *
         * @param v The View that was clicked
         */
        @Override
        public void onClick(View v) {
            int adapterPosition = getAdapterPosition();
            String trailerChosen = String.valueOf(mMovieTrailers.getResults().get(adapterPosition).getKey());
            mClickHandler.onClickTrailer(trailerChosen);
        }
    }

    /**
     * This gets called when each new ViewHolder is created. This happens when the RecyclerView
     * is laid out. Enough ViewHolders will be created to fill the screen and allow for scrolling.
     *
     * @param viewGroup The ViewGroup that these ViewHolders are contained within.
     * @param viewType  If your RecyclerView has more than one type of item (which ours doesn't) you
     *                  can use this viewType integer to provide a different layout.
     * @return A new ForecastAdapterViewHolder that holds the View for each list item
     */
    @Override
    public MovieTrailerAdapter.MovieTrailerAdapterViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.movie_list_trailer;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately);
        return new MovieTrailerAdapter.MovieTrailerAdapterViewHolder(view);
    }

    /**
     * OnBindViewHolder is called by the RecyclerView to display the data at the specified
     * position. In this method, we update the contents of the ViewHolder to display the movie image
     * for this particular position, using the "position" argument that is conveniently
     * passed into us.
     *
     * @param movieAdapterViewHolder The ViewHolder which should be updated to represent the
     *                                  contents of the item at the given position in the data set.
     * @param position                  The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(MovieTrailerAdapterViewHolder movieAdapterViewHolder, int position) {
        ResultsItem trailer = mMovieTrailers.getResults().get(position);
        Glide
                .with(detailActivity)
                .load("http://img.youtube.com/vi/" + trailer.getKey() + "/hqdefault.jpg")
                //.centerCrop()
                .placeholder(R.drawable.ic_launcher_movie_foreground)
                .into(movieAdapterViewHolder.mMovieImageView);
        int trailerNum = position + 1;
        movieAdapterViewHolder.tv_TrailerNum.setText("Trailer " + trailerNum);

        //Glide.with(mainActivity).load("http://goo.gl/gEgYUd").into(movieAdapterViewHolder.mMovieTextView);
        //movieAdapterViewHolder.mMovieTextView.setText(weatherForThisDay);
    }

    /**
     * This method simply returns the number of items to display. It is used behind the scenes
     * to help layout our Views and for animations.
     *
     * @return The number of items available in our forecast
     */
    @Override
    public int getItemCount() {
        if (null == mMovieTrailers) return 0;
        return mMovieTrailers.length();
    }

    /**
     * This method is used to set the movies on a MovieListAdapter if we've already
     * created one. This is handy when we get new data from the web but don't want to create a
     * new MovieListAdapter to display it.
     *
     * @param movieData The new weather data to be displayed.
     */
    public void setMovieData(MovieTrailerResponse movieData) {
        mMovieTrailers = movieData;
        notifyDataSetChanged();
    }
}
