package com.example.android.movieproject.data.MovieReviews;

import com.google.gson.annotations.SerializedName;

public class AuthorDetails{

	@SerializedName("avatar_path")
	private String avatarPath;

	@SerializedName("name")
	private String name;

	@SerializedName("rating")
	private Object rating;

	@SerializedName("username")
	private String username;

	public String getAvatarPath(){
		return avatarPath;
	}

	public String getName(){
		return name;
	}

	public Object getRating(){
		return rating;
	}

	public String getUsername(){
		return username;
	}
}