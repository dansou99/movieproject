package com.example.android.movieproject.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.example.android.movieproject.R;
import com.example.android.movieproject.fragment.DetailFragment;
import com.example.android.movieproject.fragment.MainFragment;

public class MainActivity extends AppCompatActivity  {


    private String STATE_FRAGMENT = "fragment";
    private MainFragment mainFragment;
    private DetailFragment detailFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState != null) { // saved instance state, fragment may exist
            // look up the instance that already exists by tag
            mainFragment = (MainFragment)
                    getSupportFragmentManager().findFragmentByTag("main");


        } else if (mainFragment == null) {
            // only create fragment if they haven't been instantiated already
            mainFragment = new MainFragment();
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.mainFragment, mainFragment, "main");
            transaction.addToBackStack(null);
            transaction.commit();
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        if(getSupportFragmentManager().getBackStackEntryCount()==0){
            finish();
        }
    }
}