package com.example.android.movieproject.data.MovieTrailers;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class MovieTrailerResponse {

	@SerializedName("id")
	private int id;

	@SerializedName("results")
	private List<ResultsItem> results;

	public int getId(){
		return id;
	}

	public List<ResultsItem> getResults(){
		return results;
	}

	public int length(){
		return results.size();
	}
}