package com.example.android.movieproject.data.MovieReviews;

import com.google.gson.annotations.SerializedName;

public class ResultsItem{

	@SerializedName("author_details")
	private AuthorDetails authorDetails;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("author")
	private String author;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private String id;

	@SerializedName("content")
	private String content;

	@SerializedName("url")
	private String url;

	public AuthorDetails getAuthorDetails(){
		return authorDetails;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public String getAuthor(){
		return author;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public String getId(){
		return id;
	}

	public String getContent(){
		return content;
	}

	public String getUrl(){
		return url;
	}
}