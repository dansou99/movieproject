package com.example.android.movieproject.service;

import com.example.android.movieproject.data.Movie.MovieResponse;
import com.example.android.movieproject.data.MovieReviews.MovieReviewResponse;
import com.example.android.movieproject.data.MovieSearch.MovieSearchResponse;
import com.example.android.movieproject.data.MovieTrailers.MovieTrailerResponse;
import com.example.android.movieproject.data.MovieList.OrderedMovies;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieService {

    //https://api.themoviedb.org/3/movie/550?api_key=c035547cbc72cc941750c43c50142de7
    @GET("movie/{id_movie}")
    Call<MovieResponse> fetchMovie(@Path("id_movie") String id, @Query("api_key") String api_key);

    //https://api.themoviedb.org/3/movie/popular?api_key=c035547cbc72cc941750c43c50142de7
    @GET("movie/{order}")
    Call<OrderedMovies> fetchList(@Path("order") String order, @Query("api_key") String api_key);

    @GET("movie/{id_movie}/videos")
    Call<MovieTrailerResponse> fetchMovieTrailer(@Path("id_movie") String id, @Query("api_key") String api_key);

    @GET("movie/{id_movie}/reviews")
    Call<MovieReviewResponse> fetchMovieReview(@Path("id_movie") String id, @Query("api_key") String api_key);

    @GET("search/movie")
    Call<MovieSearchResponse> fetchMovieSearch(@Query("api_key") String api_key, @Query("query") String query);
}
