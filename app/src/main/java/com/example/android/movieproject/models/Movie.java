package com.example.android.movieproject.models;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.List;

@Entity
public class Movie {

    @PrimaryKey
    public int id;

    @ColumnInfo(name = "title")
    public String title;

    @ColumnInfo(name = "backdrop_path")
    public String backdrop_path;

    @ColumnInfo(name = "release_date")
    public String release_date;

    @ColumnInfo(name = "runtime")
    public int runtime;

    @ColumnInfo(name = "vote_average")
    public double vote_average;

    @ColumnInfo(name = "overview")
    public String overview;

    @Ignore
    public Movie(int id, String backdrop_path) {
        this.id = id;
        this.backdrop_path = backdrop_path;
    }

    public Movie(int id, String title, String backdrop_path, String release_date, int runtime, double vote_average, String overview) {
        this.id = id;
        this.title = title;
        this.backdrop_path = backdrop_path;
        this.release_date = release_date;
        this.runtime = runtime;
        this.vote_average = vote_average;
        this.overview = overview;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }

    public String getRelease_date() {
        return release_date;
    }

    public void setRelease_date(String release_date) {
        this.release_date = release_date;
    }

    public int getRuntime() {
        return runtime;
    }

    public void setRuntime(int runtime) {
        this.runtime = runtime;
    }

    public double getVote_average() {
        return vote_average;
    }

    public void setVote_average(double vote_average) {
        this.vote_average = vote_average;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

}
