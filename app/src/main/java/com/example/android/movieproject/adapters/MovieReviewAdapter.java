package com.example.android.movieproject.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.android.movieproject.R;
import com.example.android.movieproject.data.MovieReviews.MovieReviewResponse;
import com.example.android.movieproject.data.MovieReviews.ResultsItem;
import com.example.android.movieproject.fragment.DetailFragment;

public class MovieReviewAdapter extends RecyclerView.Adapter<MovieReviewAdapter.MovieReviewAdapterViewHolder> {
    private MovieReviewResponse mMovieReviews;
    private DetailFragment detailActivity;



    /**
     * Creates a MovieListAdapter.
     *
     * @param main
     */
    public MovieReviewAdapter(DetailFragment main) {
        detailActivity = main;
    }


    public class MovieReviewAdapterViewHolder extends RecyclerView.ViewHolder  {
        private final TextView tv_ReviewAuthor;
        private final TextView tv_ReviewFull;

        public MovieReviewAdapterViewHolder(View view) {
            super(view);
            tv_ReviewAuthor = (TextView) view.findViewById(R.id.tv_reviewAuthor);
            tv_ReviewFull = (TextView) view.findViewById(R.id.tv_reviewFull);
        }


    }

    /**
     * This gets called when each new ViewHolder is created. This happens when the RecyclerView
     * is laid out. Enough ViewHolders will be created to fill the screen and allow for scrolling.
     *
     * @param viewGroup The ViewGroup that these ViewHolders are contained within.
     * @param viewType  If your RecyclerView has more than one type of item (which ours doesn't) you
     *                  can use this viewType integer to provide a different layout.
     * @return A new ForecastAdapterViewHolder that holds the View for each list item
     */
    @Override
    public MovieReviewAdapter.MovieReviewAdapterViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.movie_list_reviews;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately);
        return new MovieReviewAdapter.MovieReviewAdapterViewHolder(view);
    }

    /**
     * OnBindViewHolder is called by the RecyclerView to display the data at the specified
     * position. In this method, we update the contents of the ViewHolder to display the movie image
     * for this particular position, using the "position" argument that is conveniently
     * passed into us.
     *
     * @param movieAdapterViewHolder The ViewHolder which should be updated to represent the
     *                                  contents of the item at the given position in the data set.
     * @param position                  The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(MovieReviewAdapterViewHolder movieAdapterViewHolder, int position) {
        ResultsItem review = mMovieReviews.getResults().get(position);
        movieAdapterViewHolder.tv_ReviewAuthor.setText(review.getAuthor());
        movieAdapterViewHolder.tv_ReviewFull.setText(review.getContent());

        //Glide.with(mainActivity).load("http://goo.gl/gEgYUd").into(movieAdapterViewHolder.mMovieTextView);
        //movieAdapterViewHolder.mMovieTextView.setText(weatherForThisDay);
    }

    /**
     * This method simply returns the number of items to display. It is used behind the scenes
     * to help layout our Views and for animations.
     *
     * @return The number of items available in our forecast
     */
    @Override
    public int getItemCount() {
        if (null == mMovieReviews) return 0;
        return mMovieReviews.length();
    }

    /**
     * This method is used to set the movies on a MovieListAdapter if we've already
     * created one. This is handy when we get new data from the web but don't want to create a
     * new MovieListAdapter to display it.
     *
     * @param movieData The new weather data to be displayed.
     */
    public void setMovieData(MovieReviewResponse movieData) {
        mMovieReviews = movieData;
        notifyDataSetChanged();
    }
}