package com.example.android.movieproject.application;

import android.app.Application;

import androidx.room.Room;

import com.example.android.movieproject.appDatabase.AppDatabase;
import com.example.android.movieproject.service.MovieService;
import com.example.android.movieproject.utils.Constants;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MoviesApplication extends Application {

    private MovieService service;
    private AppDatabase db;

    @Override
    public void onCreate() {
        super.onCreate();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(MovieService.class);

        db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "db-name").build();

    }

    public MovieService getService(){
        return service;
    }
    public AppDatabase getDb() { return db; }


}
