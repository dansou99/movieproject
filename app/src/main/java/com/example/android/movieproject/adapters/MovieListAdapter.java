package com.example.android.movieproject.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.android.movieproject.activity.MainActivity;
import com.example.android.movieproject.R;
import com.example.android.movieproject.data.MovieList.OrderedMovies;
import com.example.android.movieproject.data.MovieList.ResultsItem;
import com.example.android.movieproject.fragment.MainFragment;
import com.example.android.movieproject.models.Movie;

import java.util.List;

public class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.MovieAdapterViewHolder> {

    private List<Movie> mMovieData;
    private MainFragment mainActivity;
    private final MovieAdapterOnClickHandler mClickHandler;

    /**
     * The interface that receives onClick messages.
     */
    public interface MovieAdapterOnClickHandler {
        void onClick(String movieSelected);
    }

    /**
     * Creates a MovieListAdapter.
     *
     * @param clickHandler The on-click handler for this adapter. This single handler is called
     *                     when an item is clicked.
     * @param main
     */
    public MovieListAdapter(MovieAdapterOnClickHandler clickHandler, MainFragment main) {
        mClickHandler = clickHandler;
        mainActivity = main;
    }


    public class MovieAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final ImageView mMovieImageView;

        public MovieAdapterViewHolder(View view) {
            super(view);
            mMovieImageView = (ImageView) view.findViewById(R.id.tv_movie_data);
            view.setOnClickListener(this);
        }

        /**
         * This gets called by the child views during a click.
         *
         * @param v The View that was clicked
         */
        @Override
        public void onClick(View v) {
            int adapterPosition = getAdapterPosition();
            String movieChosen = String.valueOf(mMovieData.get(adapterPosition).getId());
            mClickHandler.onClick(movieChosen);
        }
    }

    /**
     * This gets called when each new ViewHolder is created. This happens when the RecyclerView
     * is laid out. Enough ViewHolders will be created to fill the screen and allow for scrolling.
     *
     * @param viewGroup The ViewGroup that these ViewHolders are contained within.
     * @param viewType  If your RecyclerView has more than one type of item (which ours doesn't) you
     *                  can use this viewType integer to provide a different layout.
     * @return A new ForecastAdapterViewHolder that holds the View for each list item
     */
    @Override
    public MovieAdapterViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        Context context = viewGroup.getContext();
        int layoutIdForListItem = R.layout.movie_list_item;
        LayoutInflater inflater = LayoutInflater.from(context);
        boolean shouldAttachToParentImmediately = false;

        View view = inflater.inflate(layoutIdForListItem, viewGroup, shouldAttachToParentImmediately);
        return new MovieAdapterViewHolder(view);
    }

    /**
     * OnBindViewHolder is called by the RecyclerView to display the data at the specified
     * position. In this method, we update the contents of the ViewHolder to display the movie image
     * for this particular position, using the "position" argument that is conveniently
     * passed into us.
     *
     * @param movieAdapterViewHolder The ViewHolder which should be updated to represent the
     *                                  contents of the item at the given position in the data set.
     * @param position                  The position of the item within the adapter's data set.
     */
    @Override
    public void onBindViewHolder(MovieAdapterViewHolder movieAdapterViewHolder, int position) {
        Movie movie = mMovieData.get(position);
        Glide
                .with(mainActivity)
                .load("https://image.tmdb.org/t/p/original/" + movie.getBackdrop_path())
                //.centerCrop()
                .apply(new RequestOptions().override(550, 600))
                .placeholder(R.drawable.ic_launcher_movie_foreground)
                .into(movieAdapterViewHolder.mMovieImageView);

        //Glide.with(mainActivity).load("http://goo.gl/gEgYUd").into(movieAdapterViewHolder.mMovieTextView);
        //movieAdapterViewHolder.mMovieTextView.setText(weatherForThisDay);
    }

    /**
     * This method simply returns the number of items to display. It is used behind the scenes
     * to help layout our Views and for animations.
     *
     * @return The number of items available in our forecast
     */
    @Override
    public int getItemCount() {
        if (null == mMovieData) return 0;
        return mMovieData.size();
    }

    /**
     * This method is used to set the movies on a MovieListAdapter if we've already
     * created one. This is handy when we get new data from the web but don't want to create a
     * new MovieListAdapter to display it.
     *
     * @param movieData The new weather data to be displayed.
     */
    public void setMovieData(List<Movie> movieData) {
        mMovieData = movieData;
        notifyDataSetChanged();
    }

}
