package com.example.android.movieproject.fragment;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.android.movieproject.BuildConfig;
import com.example.android.movieproject.R;
import com.example.android.movieproject.adapters.MovieReviewAdapter;
import com.example.android.movieproject.adapters.MovieTrailerAdapter;
import com.example.android.movieproject.appDatabase.AppDatabase;
import com.example.android.movieproject.application.MoviesApplication;
import com.example.android.movieproject.data.Movie.MovieResponse;
import com.example.android.movieproject.data.MovieReviews.MovieReviewResponse;
import com.example.android.movieproject.data.MovieTrailers.MovieTrailerResponse;
import com.example.android.movieproject.models.Movie;
import com.example.android.movieproject.service.MovieService;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailFragment extends Fragment implements MovieTrailerAdapter.MovieTrailerAdapterOnClickHandler {

    private String movieChosen;
    private String STATE_MOVIE_CHOSEN = "test";
    private int movieChosenInt;
    private Movie movie;
    private Movie movieTemp;
    FragmentActivity activity;
    private TextView tv_MovieTitle;
    private ImageView iv_MoviePoster;
    private TextView tv_MovieRelease;
    private TextView tv_MovieRuntime;
    private TextView tv_MovieRating;
    private TextView tv_Trailer;
    private TextView tv_Review;
    private Button bt_Fav;
    private TextView tv_MovieSynopsis;
    private RecyclerView rv_MovieTrailer;
    private MovieTrailerAdapter mMovieTrailerAdapter;
    private RecyclerView rv_MovieReview;
    private MovieReviewAdapter mMovieReviewAdapter;
    private ProgressBar pb_LoadingIndicator;
    private TextView tv_MovieDetailError;
    private LinearLayout ll_DetailMovie;
    private AppDatabase db;
    private MovieService service;

    public DetailFragment() {
        // doesn't do anything special
    }

    public DetailFragment(String movieSelected){
        setRetainInstance(true);
        movieChosen = movieSelected;
        setHasOptionsMenu(false);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_detail, null);
        setHasOptionsMenu(false);
        setRetainInstance(true);

        if (savedInstanceState != null) {
            // Restore value of members from saved state
            movieChosen = savedInstanceState.getString(STATE_MOVIE_CHOSEN);
        }

        movieChosenInt = Integer.parseInt(movieChosen);

        activity = getActivity();
        db = ((MoviesApplication)getActivity().getApplication()).getDb();

        pb_LoadingIndicator = (ProgressBar) v.findViewById(R.id.pb_loading_indicator);
        tv_MovieDetailError = (TextView) v.findViewById(R.id.tv_movie_detail_error);
        ll_DetailMovie = (LinearLayout) v.findViewById(R.id.ll_DetailMovie);
        showLoading();

        tv_MovieTitle = (TextView) v.findViewById(R.id.tv_movieTitle);
        iv_MoviePoster = (ImageView) v.findViewById(R.id.iv_MoviePoster);
        tv_MovieRelease = (TextView) v.findViewById(R.id.tv_movieReleaseDate);
        tv_MovieRuntime = (TextView) v.findViewById(R.id.tv_movieRuntime);
        tv_MovieRating = (TextView) v.findViewById(R.id.tv_movieRating);
        tv_MovieSynopsis = (TextView) v.findViewById(R.id.tv_movieDescription);
        rv_MovieTrailer = (RecyclerView) v.findViewById(R.id.rv_movieTrailers);
        rv_MovieReview = (RecyclerView) v.findViewById(R.id.rv_movieReviews);
        tv_Trailer = (TextView) v.findViewById(R.id.tv_Trailer);
        tv_Review = (TextView) v.findViewById(R.id.tv_Review);
        bt_Fav = (Button) v.findViewById(R.id.bt_fav);
        buttonFavSet();
        service = ((MoviesApplication)getActivity().getApplication()).getService();

        LinearLayoutManager trailerLayoutManager
                = new LinearLayoutManager(activity,LinearLayoutManager.VERTICAL,false);
        rv_MovieTrailer.setLayoutManager(trailerLayoutManager);
        rv_MovieTrailer.setHasFixedSize(true);
        mMovieTrailerAdapter = new MovieTrailerAdapter(this,this);
        rv_MovieTrailer.setAdapter(mMovieTrailerAdapter);

        LinearLayoutManager reviewLayoutManager
                = new LinearLayoutManager(activity,LinearLayoutManager.VERTICAL,false);
        rv_MovieReview.setLayoutManager(reviewLayoutManager);
        rv_MovieReview.setHasFixedSize(true);
        mMovieReviewAdapter = new MovieReviewAdapter(this);
        rv_MovieReview.setAdapter(mMovieReviewAdapter);

        fillMovieInfo();


        bt_Fav.setOnClickListener(a -> {
            Executor executor = Executors.newSingleThreadExecutor();
            executor.execute(() -> {
                Movie test = db.movieDao().getMovieById(movieChosenInt);
                if(test == null) {
                    db.movieDao().insertAll(movie);
                } else {
                    db.movieDao().deleteMovieById(movieChosenInt);
                    /*FragmentManager fm = getActivity().getSupportFragmentManager();
                    MainFragment fragment = (MainFragment)fm.findFragmentByTag("main");
                    fragment.loadLocalMovieData();*/
                }
                buttonFavSet();
            });

        });

        return v;
    }

    private void buttonFavSet() {
        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            Movie test = db.movieDao().getMovieById(Integer.parseInt(movieChosen));
            getActivity().runOnUiThread(() -> {
                if(test == null) {
                    bt_Fav.setText("Mark as favorite");
                } else {
                    bt_Fav.setText("Remove as favorite");
                }
            });
        });
    }

    private void fillMovieInfo() {
        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(() -> {
            movieTemp = db.movieDao().getMovieById(movieChosenInt);
            getActivity().runOnUiThread(() -> {
                if (movieTemp == null) {
                    service.fetchMovie(movieChosen, BuildConfig.TMDB_KEY).enqueue(new Callback<MovieResponse>() {
                        @Override
                        public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                            MovieResponse movieResponse = response.body();
                            movie = new Movie
                                    (movieResponse.getId(), movieResponse.getTitle(),
                                            movieResponse.getBackdropPath(), movieResponse.getReleaseDate(),
                                            movieResponse.getRuntime(), movieResponse.getVoteAverage(),
                                            movieResponse.getOverview());

                            fillUIDetails(movie);
                            showMovieDataView();

                        }

                        @Override
                        public void onFailure(Call<MovieResponse> call, Throwable t) {
                            showError();
                        }
                    });
                } else {
                    movie = movieTemp;
                    fillUIDetails(movie);
                    showMovieDataView();
                }
            });

        });







        service.fetchMovieTrailer(movieChosen, BuildConfig.TMDB_KEY).enqueue(new Callback<MovieTrailerResponse>() {
            @Override
            public void onResponse(Call<MovieTrailerResponse> call, Response<MovieTrailerResponse> response) {
                MovieTrailerResponse movieTrailers = response.body();
                if(movieTrailers.getResults().size() != 0) {
                    mMovieTrailerAdapter.setMovieData(movieTrailers);
                } else {
                    getActivity().runOnUiThread(() -> {
                        tv_Trailer.setText("Trailers: No available trailers at this time");
                    });
                }
            }

            @Override
            public void onFailure(Call<MovieTrailerResponse> call, Throwable t) {
                tv_Trailer.setText("Trailers: No available trailers at this time");
            }
        });

        service.fetchMovieReview(movieChosen, BuildConfig.TMDB_KEY).enqueue(new Callback<MovieReviewResponse>() {
            @Override
            public void onResponse(Call<MovieReviewResponse> call, Response<MovieReviewResponse> response) {
                MovieReviewResponse movieReviews = response.body();
                if(movieReviews.getResults().size() != 0) {
                    mMovieReviewAdapter.setMovieData(movieReviews);
                } else {
                    getActivity().runOnUiThread(() -> {
                        tv_Review.setText("Reviews: No available reviews at this time");
                    });
                }
            }

            @Override
            public void onFailure(Call<MovieReviewResponse> call, Throwable t) {
                tv_Review.setText("Reviews: No available reviews at this time");
            }
        });



    }

    private void fillUIDetails(Movie movie) {
        tv_MovieTitle.setText(movie.getTitle());
        Glide
                .with(activity)
                .load("https://image.tmdb.org/t/p/original/" + movie.getBackdrop_path())
                //.centerCrop()
                .apply(new RequestOptions().override(550, 600))
                .placeholder(R.drawable.ic_launcher_movie_foreground)
                .into(iv_MoviePoster);
        tv_MovieRelease.setText(movie.getRelease_date());
        tv_MovieRuntime.setText(movie.getRuntime() + "min");
        tv_MovieRating.setText(movie.getVote_average() + "/10");
        tv_MovieSynopsis.setText(movie.getOverview());
    }

    private void showMovieDataView() {
        pb_LoadingIndicator.setVisibility(View.INVISIBLE);
        tv_MovieDetailError.setVisibility(View.INVISIBLE);
        ll_DetailMovie.setVisibility(View.VISIBLE);
    }

    private void showLoading() {
        tv_MovieDetailError.setVisibility(View.INVISIBLE);
        ll_DetailMovie.setVisibility(View.INVISIBLE);
        pb_LoadingIndicator.setVisibility(View.VISIBLE);
    }

    private void showError() {
        pb_LoadingIndicator.setVisibility(View.INVISIBLE);
        ll_DetailMovie.setVisibility(View.INVISIBLE);
        tv_MovieDetailError.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClickTrailer(String trailerSelected) {
        Intent appIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:" + trailerSelected));
        Intent webIntent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://www.youtube.com/watch?v=" + trailerSelected));
        try {
            this.startActivity(appIntent);
        } catch (ActivityNotFoundException ex) {
            this.startActivity(webIntent);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Save the user's current game state
        savedInstanceState.putString(STATE_MOVIE_CHOSEN, movieChosen);

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

}
